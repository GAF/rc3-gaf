# README

## Lizenz
Die Karten und die selbst erstellten Tilesets stehen unter der Creative Commons BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/) Lizenz.

## genutzte Tiles
Tiles im Ordner tiles/tiles.rc3.world/ sind von https://tiles.rc3.world/. Vielen Dank an Mullana und die fleißigen Wesen von dort!

GAF-eigene Tilesets mögen im Ordner tiles/tiles.gaf/ zu finden sein.

